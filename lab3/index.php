
<h3>Links of http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/testpage.html </h3>

<?php 
// delete old txt files
unlink('results.txt');
unlink('crawl.txt');

// append first (informative) line in both txt files
file_put_contents('crawl.txt', "Links marked with a star (*) are disallowed by robots.txt and has not been visited.\r\n\n", FILE_APPEND);
file_put_contents('results.txt', "Links marked with a star (*) are disallowed by robots.txt and has not been visited.\r\n\n", FILE_APPEND);

//get all links from links http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/testpage.html with get_links() function
$links = get_links('http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/testpage.html');
foreach($links as $link):
// prints them
  echo $link['url']."<br>";
endforeach;



////// this part loads in array a with dept 5 from begining 'http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/' all links which according on if condition are:
//1. not in array
//2. starts with http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/
//3. are allowed by robots.txt or loads with * prefix if disallowed


// inicialise array
$a = array();
// loads in arrauy 1st element the root. That becomes as 1st loop 
array_push($a,'http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/');
//loop it 5 times
for ($i=0; $i<5; $i++):

foreach($a as $subA):
// get links from every element
$links = get_links($subA);
foreach($links as $link):
  // convert relative to absolute URL
  $link['url'] = rel2abs($link['url'],'http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/');
  
  
  // conditions : 1. not in aaray, 2. begins with root 3. allowed by robots
  if (!in_array($link['url'], $a) and strstr($link['url'],'http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/') and robots_allowed($link['url'])): 
		 array_push($a, $link['url']);
	 // conditions : disallowed and not in array
  elseif  (!robots_allowed($link['url']) and !in_array('* '.$link['url'], $a)):
	    
		 array_push($a, '* '.$link['url']); 
  endif;
  
endforeach;
endforeach;

endfor;
//////////////////////////////////////////////////////////

?>

<br><h3>All links with logs. Disalowed paths are marked with star (*) and not been crowled</h3>

<?php
////// now we have array with unique links in depth 5 from root.
// next step is to faind all links inside them if it is allowed by robots.txt

foreach($a as $subA):
$i=0;
// write array element
echo '<br><br>'.$subA.'<br>';
// write in file 
file_put_contents('crawl.txt', "<Visited: ".$subA.">\r\n", FILE_APPEND);
file_put_contents('results.txt', "<Visited: ".$subA.">\r\n", FILE_APPEND);
$links = get_links($subA);
foreach($links as $link):
  // convert to absolute URL
  $link['url'] = rel2abs($link['url'],'http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/');
  // check if crowling is allowed
  if (robots_allowed($link['url'])): 
		 //array_push($a, $link['url']);
		 echo '&nbsp;&nbsp;&nbsp;'.$link['url'].'<br>';
         file_put_contents('crawl.txt', "  <".$link['url'].">\r\n", FILE_APPEND);
		 //counter for visited links
		 $i++;		 
  endif;
endforeach;
  echo '&nbsp;&nbsp;&nbsp;Number of links in visited page: '.$i.'<br>';
  file_put_contents('results.txt', "  <Number of links to visited pages: ".$i.">\r\n", FILE_APPEND);
endforeach;



// links to both reports
?>
<br><br>
<a href="crawl.txt">crawl.txt</a><br>
<a href="results.txt">results.txt</a><br>

<?php


////////////////////////////////////////////////////////////////////////////////FUNCTIONS



 // getting all links from passed url
   function get_links($url)
  {
	  // inicialise DOM object
    $xml = new DOMDocument();
    @$xml->loadHTMLFile($url);
	// inicialise array
    $links = array();
	// load array with links
    foreach($xml->getElementsByTagName('a') as $link): 
        $links[] = array('url' => $link->getAttribute('href'), 'text' => $link->nodeValue);
	endforeach;	
	// returns array
	return $links;
	}
 


  function robots_allowed($url, $useragent=false)
  {
    // parse url to retrieve host and path
    $parsed = parse_url($url);
    $agents = array(preg_quote('*'));
    if($useragent) $agents[] = preg_quote($useragent);
    $agents = implode('|', $agents);

    // location of robots.txt file
    $robotstxt = @file("http://www.dcs.bbk.ac.uk/~martin/sewn/ls3/robots.txt");
    // if there isn't a robots, then we're allowed in
    if(empty($robotstxt)) return true;

    $rules = array();
    $ruleApplies = false;
    foreach($robotstxt as $line) {
      // skip blank lines
      if(!$line = trim($line)) continue;

      // following rules only apply if User-agent matches $useragent or '*'
      if(preg_match('/^\s*User-agent: (.*)/i', $line, $match)) {
        $ruleApplies = preg_match("/($agents)/i", $match[1]);
      }
      if($ruleApplies && preg_match('/^\s*Disallow:(.*)/i', $line, $regs)) {
        // an empty rule implies full access - no further tests required
        if(!$regs[1]) return true;
        // add rules that apply to array for testing
        $rules[] = preg_quote(trim($regs[1]), '/');
      }
    }

    foreach($rules as $rule) {
      // check if page is disallowed 
	  $rule='\/\~martin\/sewn\/ls3'.$rule;
      if(preg_match("/^$rule/", $parsed['path']))  return false;
    }

    // page is not disallowed
    return true;
  }
  
  
  
  
  // transfers relative url to absolute according on regular expressions
 function rel2abs($rel, $base){  
     if (parse_url($rel, PHP_URL_SCHEME) != '')   
	  return $rel;  else if ($rel[0] == '#' || $rel[0] == '?')   
	   return $base.$rel;  extract(parse_url($base)); 
	    $abs = ($rel[0] == '/' ? '' : preg_replace('#/[^/]*$#', '', $path))."/$rel";  
		$re  = array('#(/\.?/)#', '#/(?!\.\.)[^/]+/\.\./#');  for ($n = 1; $n > 0; 
		$abs = preg_replace($re, '/', $abs, -1, $n));  
	   return $scheme.'://'.$host.str_replace('../', '', $abs);}


?>
 


